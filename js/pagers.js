window.onscroll = function(){
    document.getElementById("nav-form").style.top = window.pageYOffset + "px";
};

window.addEventListener("scroll", function(){
    if (window.pageYOffset > 500) {
        document.getElementById("goto-top").style.display = 'block';
    } else {
        document.getElementById("goto-top").style.display = 'none';
    }
});

var { groups, subgroups, items } = {
    "groups": [
        { "title": "Наземный транспорт", "id": 1 },
        { "title": "Водный транспорт", "id": 2 },
        { "title": "Воздушный транспорт", "id": 3 }
    ],
    "subgroups": [
        { "title": "Железнодорожный транспорт", "groupId": 1, "id": 1 },
        { "title": "Автомобильный транспорт", "groupId": 1, "id": 2 },
        { "title": "Ручной транспорт", "groupId": 1, "id": 3 },

        { "title": "Речной транспорт", "groupId": 2, "id": 4 },
        { "title": "Морской транспорт", "groupId": 2, "id": 5 },
        { "title": "Подводный транспорт", "groupId": 2, "id": 6 },

        { "title": "Самолеты", "groupId": 3, "id": 7 },
        { "title": "Вертолеты", "groupId": 3, "id": 8 },
        { "title": "Ракета (шаттл)", "groupId": 3, "id": 9 }
    ],
    "items": [
        { "title": "Дизельный поезд", "template": "dizel.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Дрезина", "template": "drezina.html", "subGroupId": 1 },
        { "title": "Электропоезд", "template": "electropoezd.html", "subGroupId": 1 },

        { "title": "Грузовой автомобиль", "template": "gruzovoy.html", "subGroupId": 2 },
        { "title": "Автобус", "template": "avtobus.html", "subGroupId": 2 },

        { "title": "Тачка", "template": "tachka.html", "subGroupId": 3 },
        { "title": "Тележка", "template": "telezhka.html", "subGroupId": 3 },
        { "title": "Велосипед", "template": "velosiped.html", "subGroupId": 3 },

        { "title": "Трамвай", "template": "tramvay.html", "subGroupId": 4 },
        { "title": "Теплоход", "template": "teplohod.html", "subGroupId": 4 },

        { "title": "Крейсер", "template": "kreyser.html", "subGroupId": 5 },
        { "title": "Круизный лайнер", "template": "layner.html", "subGroupId": 5 },
        { "title": "Баржа", "template": "barzha.html", "subGroupId": 5 },

        { "title": "Подводная лодка", "template": "podlodka.html", "subGroupId": 6 },
        { "title": "Батискаф", "template": "batiskaf.html", "subGroupId": 6 },
        { "title": "Капсула смерти", "template": "kapsula_smerti.html", "subGroupId": 6 },

        { "title": "Боинг", "template": "Boing.html", "subGroupId": 7 },
        { "title": "Аэробус", "template": "aerobus.html", "subGroupId": 7 },
        { "title": "Руслан", "template": "Ruslan.html", "subGroupId": 7 },

        { "title": "МИ", "template": "mi.html", "subGroupId": 8 },
        { "title": "Апач", "template": "apach.html", "subGroupId": 8 },
        { "title": "Черная акула", "template": "akula.html", "subGroupId": 8 },

        { "title": "Союз", "template": "soyuz.html", "subGroupId": 9 },
        { "title": "Апполон", "template": "apolon.html", "subGroupId": 9 },
        { "title": "Дискавери", "template": "diskaveri.html", "subGroupId": 9 },
        { "title": "Буран", "template": "buran.html", "subGroupId": 9 }
    ]
};


$("#groups").html(makeOptions("groupsSelect", groups, "Выберите тип транспорта"));

$("#groupsSelect").change(function(e){
    e.preventDefault();
    var groupId = $(this).children("option:selected").val();

    if (groupId) {
        $("#subgroups").html(
            makeOptions(
                "subgroupsSelect",
                subgroups.filter(function(subgroupItem){
                    return subgroupItem.groupId == groupId;
                }),
                "Выберите вид транспорта"
            )
        );
        document.getElementById("items").innerHTML = "";
        clearPageContent();
    } else {
        document.getElementById("subgroups").innerHTML = "";
        document.getElementById("items").innerHTML = "";
        clearPageContent();
    }

    var subGroupsSelect = document.getElementById("subgroupsSelect");
    if (subGroupsSelect) {
        subGroupsSelect.addEventListener("change", function(){
            var subgroupId = $(this).children("option:selected").val();
           
            var forLinksContent = document.getElementById("page-content");
            var content = '';
            $.each(items.filter(function(item){
                return item.subGroupId == subgroupId;
            }), function(idx, item){
                content += '<h3 id="' + item.template + '">' + item.title + '</h3><p><object style="width: 100%;" type="text/html" data="pages/' + item.template + '"></object></p><hr>';
            });
            content = content.slice(0, -4);
            forLinksContent.innerHTML = content;

            if (subgroupId) {
                $("#items").html(
                    makeOptions(
                        "itemsSelect",
                        items.filter(function(item){
                            return item.subGroupId == subgroupId;
                        }),
                        "Выберите статью"
                    )
                );

                var itemsSelect = document.getElementById("itemsSelect");
                if (itemsSelect) {
                    itemsSelect.addEventListener("change", function(){
                        var anchorTitle = this.options[this.selectedIndex].value;
                        if (anchorTitle) {
                            location.hash = "#" + anchorTitle;
                        } else {
                            clearPageContent();
                        }
                    });
                } else {
                    clearPageContent();
                }

            } else {
                document.getElementById("items").innerHTML = "";
                clearPageContent();
            }
        });
    }

});


function makeOptions(id, items, initPlaceholder) {
    var html = "<select class='form-control' id=" + id + "><option value=''>" + initPlaceholder + "</option>";
    $.each(items, function(idx, item){
        var value = item.id ? item.id : item.template;
        html+= "<option value='" + value + "'>" + item.title + "</option>"
    });
    html += "</select>";
    return html;
}

function clearPageContent(){
    $("#page-content").html("Для просмотра данных о товаре, выберите его из навигационного меню слева...");
}

function loadItemWithObject(template) {
    $('#items option[value="' + template + '"]').prop('selected', true);

    var contentObject = '<object style="width: 100%; height: 100vh;" type="text/html" data="pages/' + template + '"></object>';
    document.getElementById("page-content").innerHTML = contentObject;
}
